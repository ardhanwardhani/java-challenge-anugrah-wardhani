package ist.challenge.anugrah_wardhani.template.repository;

import ist.challenge.anugrah_wardhani.template.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    public Optional<User> findByUsername(String username);
}
