package ist.challenge.anugrah_wardhani.template.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SwapiService {
    @Value("${swapi.base-url}")
    private String swapiBaseUrl;

    private final RestTemplate restTemplate;

    public SwapiService(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    public String getStarWarsCharacter(int characterId){
        String url = swapiBaseUrl + "/people/" + characterId;
        return restTemplate.getForObject(url, String.class);
    }

    public String getStarWarsFilms(){
        String url = swapiBaseUrl + "/films";
        return restTemplate.getForObject(url, String.class);
    }
}
