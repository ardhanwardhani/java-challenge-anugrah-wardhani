package ist.challenge.anugrah_wardhani.template.service;

import ist.challenge.anugrah_wardhani.template.constant.Constant;
import ist.challenge.anugrah_wardhani.template.dto.UserDTO;
import ist.challenge.anugrah_wardhani.template.model.User;
import ist.challenge.anugrah_wardhani.template.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }
    public void createUser(UserDTO request){
        try {
            Optional<User> existingUser = userRepository.findByUsername(request.getUsername());

            if(existingUser.isEmpty()){
                User user = new User();
                user.setUsername(request.getUsername());
                user.setPassword(request.getPassword());
                userRepository.save(user);
            } else {
                throw new ResponseStatusException(HttpStatus.CONFLICT, Constant.USERNAME_ALREADY_REGISTERED);
            }
        } catch (ResponseStatusException ex) {
            throw ex;
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, Constant.INTERNAL_SERVER_ERROR, e);
        }
    }

    public void login(UserDTO request){
        try{
            String username = request.getUsername();
            String password = request.getPassword();

            if (username == null || username.isEmpty() || password == null || password.isEmpty()){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constant.FIELD_MUST_NOT_BE_BLANK);
            }

            Optional<User> user = userRepository.findByUsername(username);

            if (user.isEmpty() || !user.get().getPassword().equals(password)){
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, Constant.DATA_DOES_NOT_MATCH);
            }
        }catch (ResponseStatusException ex){
            throw ex;
        }catch(Exception e){
            throw new RuntimeException("Failed to login: " + e.getMessage(), e);
        }
    }

    public List<User> userList(){
        return userRepository.findAll();
    }

    public void editUser(Long userId, UserDTO request) {
        try {
            Optional<User> existingUser = userRepository.findById(userId);

            if (existingUser.isPresent()) {
                User user = existingUser.get();

                Optional<User> userWithEditedUsername = userRepository.findByUsername(request.getUsername());
                if (userWithEditedUsername.isPresent() && !userWithEditedUsername.get().getId().equals(userId)) {
                    throw new ResponseStatusException(HttpStatus.CONFLICT, Constant.USERNAME_ALREADY_REGISTERED);
                }

                if (request.getPassword().equals(user.getPassword())) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, Constant.PASSWORD_MUST_NOT_SAME);
                }

                user.setUsername(request.getUsername());
                user.setPassword(request.getPassword());
                userRepository.save(user);
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constant.USER_NOT_FOUND);
            }
        } catch (ResponseStatusException ex) {
            throw ex;
        } catch (Exception e) {
            throw new RuntimeException("Failed to edit user: " + e.getMessage(), e);
        }
    }
}
