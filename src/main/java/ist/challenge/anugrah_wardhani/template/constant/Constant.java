package ist.challenge.anugrah_wardhani.template.constant;

public class Constant {
    private Constant(){

    }
    public static final String USERNAME_ALREADY_REGISTERED = "Username sudah terpakai";
    public static final String REGISTER_SUCCESSFULLY = "User berhasil dibuat";
    public static final String LOGIN_SUCCESSFULLY = "Sukses Login";
    public static final String FIELD_MUST_NOT_BE_BLANK = "Username dan/atau Password kosong";
    public static final String DATA_DOES_NOT_MATCH = "Username / Password tidak cocok";
    public static final String INTERNAL_SERVER_ERROR = "Telah terjadi galat";
    public static final String PASSWORD_MUST_NOT_SAME = "Password tidak boleh sama dengan password sebelumnya";
    public static final String USER_NOT_FOUND = "User tidak ditemukan";
    public static final String USER_HAS_BEEN_CHANGE = "User berhasil diubah";
}
