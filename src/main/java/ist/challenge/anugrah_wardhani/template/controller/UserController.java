package ist.challenge.anugrah_wardhani.template.controller;

import ist.challenge.anugrah_wardhani.template.constant.Constant;
import ist.challenge.anugrah_wardhani.template.dto.UserDTO;
import ist.challenge.anugrah_wardhani.template.model.User;
import ist.challenge.anugrah_wardhani.template.service.UserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@Validated @RequestBody UserDTO request) {
        try {
            userService.createUser(request);
            return ResponseEntity.status(HttpStatus.CREATED).body(Constant.REGISTER_SUCCESSFULLY);
        } catch (ResponseStatusException ex) {
            return ResponseEntity.status(ex.getStatusCode()).body(ex.getReason());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Constant.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@Validated @RequestBody UserDTO request) {
        try {
            userService.login(request);
            return ResponseEntity.status(HttpStatus.OK).body(Constant.LOGIN_SUCCESSFULLY);
        } catch (ResponseStatusException ex) {
            return ResponseEntity.status(ex.getStatusCode()).body(ex.getReason());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Constant.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/list")
    public ResponseEntity<List<User>> getAllUser(){
        return ResponseEntity.ok().body(userService.userList());
    }

    @PutMapping("/edit/{userId}")
    public ResponseEntity<String> editUser(
            @PathVariable Long userId,
            @Validated @RequestBody UserDTO request
    ) {
        try {
            userService.editUser(userId, request);
            return ResponseEntity.status(HttpStatus.CREATED).body(Constant.USER_HAS_BEEN_CHANGE);
        } catch (ResponseStatusException ex) {
            return ResponseEntity.status(ex.getStatusCode()).body(ex.getReason());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gagal mengubah user: " + e.getMessage());
        }
    }
}
