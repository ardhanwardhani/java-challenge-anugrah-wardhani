package ist.challenge.anugrah_wardhani.template.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ist.challenge.anugrah_wardhani.template.service.SwapiService;

@RestController
@RequestMapping("/api/starwars")
public class StarWarsController {
    private final SwapiService swapiService;

    @Autowired
    public StarWarsController(SwapiService swapiService){
        this.swapiService = swapiService;
    }

    @GetMapping("/character/{id}")
    public String getStarWarsCharacter(@PathVariable int id){
        return swapiService.getStarWarsCharacter(id);
    }

    @GetMapping("/films-list")
    public String getAllStarWarsFilms(){
        return swapiService.getStarWarsFilms();
    }
}
