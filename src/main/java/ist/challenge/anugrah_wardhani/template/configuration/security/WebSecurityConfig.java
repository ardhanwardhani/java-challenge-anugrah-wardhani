package ist.challenge.anugrah_wardhani.template.configuration.security;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {
        @Bean
        public SecurityFilterChain configure(HttpSecurity http) throws Exception {
                http
                    .authorizeHttpRequests(config -> config
                        .anyRequest().permitAll())
                    .cors(configurer -> configurer
                        .configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues()))
                    .csrf(AbstractHttpConfigurer::disable);
                return http.build();
        }
}
