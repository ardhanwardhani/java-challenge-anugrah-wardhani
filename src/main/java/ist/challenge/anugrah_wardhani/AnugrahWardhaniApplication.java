package ist.challenge.anugrah_wardhani;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnugrahWardhaniApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnugrahWardhaniApplication.class, args);
	}

}
