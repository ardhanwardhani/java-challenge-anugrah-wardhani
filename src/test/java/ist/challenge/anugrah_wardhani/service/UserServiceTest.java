package ist.challenge.anugrah_wardhani.service;

import ist.challenge.anugrah_wardhani.template.constant.Constant;
import ist.challenge.anugrah_wardhani.template.dto.UserDTO;
import ist.challenge.anugrah_wardhani.template.model.User;
import ist.challenge.anugrah_wardhani.template.repository.UserRepository;
import ist.challenge.anugrah_wardhani.template.service.UserService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    void testCreateUser_Success() {
        UserDTO userDTO = new UserDTO("newUser", "password");
        when(userRepository.findByUsername("newUser")).thenReturn(Optional.empty());

        assertDoesNotThrow(() -> userService.createUser(userDTO));

        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    void testCreateUser_UsernameAlreadyRegistered() {
        UserDTO userDTO = new UserDTO("existingUser", "password");
        when(userRepository.findByUsername("existingUser")).thenReturn(Optional.of(new User()));

        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> userService.createUser(userDTO));
        assertEquals(HttpStatus.CONFLICT, exception.getStatusCode());
        assertEquals(Constant.USERNAME_ALREADY_REGISTERED, exception.getReason());
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    void testLogin_Success() {
        String username = "existingUser";
        String password = "correctPassword";
        UserDTO userDTO = new UserDTO(username, password);
        User existingUser = new User(1L, username, password);
        when(userRepository.findByUsername(username)).thenReturn(Optional.of(existingUser));

        assertDoesNotThrow(() -> userService.login(userDTO));
    }

    @Test
    void testLogin_BlankUsernameOrPassword() {
        String username = "";
        String password = "password";
        UserDTO userDTO = new UserDTO(username, password);

        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> userService.login(userDTO));
        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatusCode());
        assertEquals(Constant.FIELD_MUST_NOT_BE_BLANK, exception.getReason());
        verify(userRepository, never()).findByUsername(any());
    }

    @Test
    void testLogin_UserOrPasswordDoesntMatch() {
        String username = "existingUser";
        String password = "incorrectPassword";
        UserDTO userDTO = new UserDTO(username, password);
        when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> userService.login(userDTO));
        assertEquals(HttpStatus.UNAUTHORIZED, exception.getStatusCode());
        assertEquals(Constant.DATA_DOES_NOT_MATCH, exception.getReason());
        verify(userRepository, times(1)).findByUsername(username);
    }

    @Test
    void testEditUser_Success() {
        Long userId = 1L;
        String newUsername = "newUsername";
        String newPassword = "newPassword";
        UserDTO userDTO = new UserDTO(newUsername, newPassword);
        User existingUser = new User(userId, "oldUsername", "oldPassword");
        when(userRepository.findById(userId)).thenReturn(Optional.of(existingUser));
        when(userRepository.findByUsername(newUsername)).thenReturn(Optional.empty());

        assertDoesNotThrow(() -> userService.editUser(userId, userDTO));

        verify(userRepository, times(1)).findByUsername(newUsername);
        verify(userRepository, times(1)).findById(userId);
        verify(userRepository, times(1)).save(existingUser);
    }

    @Test
    void testEditUser_UsernameAlreadyUsed() {
        Long userId = 1L;
        String newUsername = "existingUsername";
        String newPassword = "newPassword";
        UserDTO userDTO = new UserDTO(newUsername, newPassword);
        User existingUser = new User(userId, "oldUsername", "oldPassword");
        when(userRepository.findById(userId)).thenReturn(Optional.of(existingUser));
        when(userRepository.findByUsername(newUsername)).thenReturn(Optional.of(new User()));

        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> userService.editUser(userId, userDTO));
        assertEquals(HttpStatus.CONFLICT, exception.getStatusCode());
        assertEquals(Constant.USERNAME_ALREADY_REGISTERED, exception.getReason());
        verify(userRepository, times(1)).findByUsername(newUsername);
        verify(userRepository, times(1)).findById(userId);
        verify(userRepository, never()).save(any());
    }

    @Test
    void testEditUser_PasswordNotSameAsBefore() {
        Long userId = 1L;
        String newUsername = "newUsername";
        String newPassword = "oldPassword";
        UserDTO userDTO = new UserDTO(newUsername, newPassword);
        User existingUser = new User(userId, "oldUsername", "oldPassword");
        when(userRepository.findById(userId)).thenReturn(Optional.of(existingUser));
        when(userRepository.findByUsername(newUsername)).thenReturn(Optional.empty());

        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> userService.editUser(userId, userDTO));
        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatusCode());
        assertEquals(Constant.PASSWORD_MUST_NOT_SAME, exception.getReason());
        verify(userRepository, times(1)).findById(userId);
        verify(userRepository, never()).save(any());
    }
}
